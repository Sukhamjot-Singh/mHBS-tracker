package dhis2.org.analytics.charts.formatters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u000e\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0002"}, d2 = {"EMPTY_LABEL", "", "dhis_android_analytics_dhisDebug"})
public final class DateLabelFormatterKt {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EMPTY_LABEL = "-";
}