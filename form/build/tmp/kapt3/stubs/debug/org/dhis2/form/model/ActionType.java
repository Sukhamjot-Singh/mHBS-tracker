package org.dhis2.form.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006\u00a8\u0006\u0007"}, d2 = {"Lorg/dhis2/form/model/ActionType;", "", "(Ljava/lang/String;I)V", "ON_NEXT", "ON_FOCUS", "ON_SAVE", "ON_TEXT_CHANGE", "form_debug"})
public enum ActionType {
    /*public static final*/ ON_NEXT /* = new ON_NEXT() */,
    /*public static final*/ ON_FOCUS /* = new ON_FOCUS() */,
    /*public static final*/ ON_SAVE /* = new ON_SAVE() */,
    /*public static final*/ ON_TEXT_CHANGE /* = new ON_TEXT_CHANGE() */;
    
    ActionType() {
    }
}